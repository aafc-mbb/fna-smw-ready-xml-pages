import json
import sys

from misc import file_to_json
from os import walk


from collections import defaultdict
d = defaultdict(int)
s = set(file_to_json("gn_blacklist.json"))


for root, dirs, files in walk("taxon_names", topdown=False):
    for file in files:
        if file.endswith("_titles.json"):
            continue
        elif file.endswith(".txt") is False:
            j = file_to_json(file, file_dir=root)["names"]
            for i in j:
                if i["type"] == "Trinomial":
                    nme = i["name"].split()
                    if nme[-2][-1] != "." and nme[-2] != "forma":
                        if nme[-1] not in s:
                            d[nme[-1]] += 1

l = [i[0] for i in sorted(d.items(), key=lambda x: -x[1])]
sys.stdout.buffer.write(json.dumps(
    l, ensure_ascii=False, indent=4).encode('utf8'))
