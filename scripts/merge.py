import json
import sys

from misc import file_to_json
from os import walk

blacklist = set(file_to_json("gn_blacklist.json"))

s = set()
for root, dirs, files in walk("taxon_names", topdown=False):
    for file in files:
        if file.endswith("_titles.json"):
            s.update([j for i in file_to_json(file, file_dir=root) for j in (lambda x: [' '.join(
                x), x[0][0] + ". " + ' '.join(x[1:])] if (len(x) > 1 and x[0][0].isupper()) else [' '.join(x)])(i.strip().split())])
        elif file.endswith("_gnfound.json"):
            s.update([i["name"] for i in file_to_json(file, file_dir=root)["names"] if not (
                (i["name"].strip().split()[-1] in blacklist) or ("�" in i["name"]))])

tmp_set = set()

for i in s:
    name = i.strip().split()
    if len(name) == 1 and len(name[0]) >= 4 and name[0][-4:] == "ceae":
        continue
    if name[0] == "Varieties":
        continue
    if len(name) > 1 and len(name[0]) > 2:
        tmp_set.add(name[0])
    if "var." in name:
        tmp_set.add(' '.join(name[name.index("var."):]))
    if "subsp." in name:
        tmp_set.add(' '.join(name[name.index("subsp."):]))
    if "×" in name:
        tmp_set.add(' '.join(name[name.index("×"):]))
    tmp_set.add(i)

# Nevada seems to be wrong more often than it is right, so removing it. Other
# incorrect names can be removed here, too
#italics_blacklist = ["Nevada", "Shrubs", "Mexico", "Fertile", "County",
                #     "Florida", "America", "Latin", "Texas", "Such", "Mature",
                #     "American", "All", "Flora", "Valley", "Mountain",
                #     "Russian", "Los", "Benson", "Bay", "Rydberg", "Diablo"]
# Florida is a genus of herons, America is a genus of animals, Virginia is a
# genus of animals. Costa is a genus of animals

# (.*)([A-Z][a-z]*<\/i>\sCounty)(.*)
#for blacklisted_word in italics_blacklist:
#    if blacklisted_word in tmp_set:
#        tmp_set.remove(blacklisted_word)

# TODO: Add whitelist: Hydrocallis, Auriculastrum, etc. Found by
# searching treatments.xml for </i> subg.

l = list(tmp_set)

sys.stdout.buffer.write(json.dumps(
    l, ensure_ascii=False, indent=4).encode('utf8'))
