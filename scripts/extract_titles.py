import io
import json
import re
import sys


input_stream = io.TextIOWrapper(sys.stdin.buffer, encoding='utf-8')

i = input_stream.read()


def add_space_before_hybrid_symbol(match):
    words = match.split(" ")
    if len(words) > 1:
        if words[1][0] == "×":
            spaced_title = words[0] + " × " + words[1][1:]
            return(spaced_title)


def append_hybrid_titles(matches):
    spaced_titles = []
    for match in matches:
        if "×" in match:
            spaced_title = add_space_before_hybrid_symbol(match)
            if spaced_title is not None:
                spaced_titles.append(spaced_title)
    matches.extend(spaced_titles)


title_match = re.compile('(?<=<title>).*?(?=\</title>)')
matches = title_match.findall(i)
append_hybrid_titles(matches)
sys.stdout.buffer.write(json.dumps(matches, ensure_ascii=False).encode('utf8'))
