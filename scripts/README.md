# Italicization Scripts

Looks for scientific names in a text and italicizes them. 

### Dependencies

```
Python 3
gnfinder (optional)
```
 
##How to Use

The scripts read a list of scientific names from a file called `full_list.json`. There is one currently included in the repository. 

The scrips do not italicize words found in `do_not_italicize.json`. Feel free to add or remove entries to this.

###Generating the Name List

If you are running the scripts on new files, you may want to generate a fresh list of valid scientific names.

Doing this can incorporate gnfinder (installed as a command line app). To do this, follow the instructions on the [gnfinder project page](https://github.com/gnames/gnfinder). 

To re-generate the name list, i.e. `full_list.json`, with gnfinder, run `./get_names.sh`. 

If you do not want to use gnfinder, instead run `./no_gnfinder.sh`.

Since gnfinder will occasionally have issues where it incorrectly identifies words as part of taxon names, there is a file included (gn_blacklist.json) which contains words that are not valid parts of taxon names but are identified as such. Please note that `gn_blacklist.json` must remain a valid json file.

If you see any entries in this file that coud be valid parts of taxon names, please remove them. Similarly, if you find any incorrectly classified words that are not on the list, please add them. To get a list of suspicious words that aren't already blacklisted, run `python3 find_suspects.py > gn_suspect_list.json`. The list will be output to a file called `gn_suspect_list.json` for review. Entries in `gn_suspect_list.json` can then be added to `gn_blacklist.json` if they are indeed not valid parts of taxon names. This is not a neccessary step.

Then, run `python3 merge.py > full_list.json`.

###Italicizing the text

To replace all files with italicized versions, run `python3 process_all.py`.

####Testing the scripts

To test the scripts, pipe a file into `process_test.py`. For example, say you copied a treatments file named `treatments.xml` into the root folders of the script directory.

Then, you could test the script on that file and pipe the output to a new file, `treatment_out.xml`, by running `python3 process_test.py < treatments.xml > treatment_out.xml`. 
