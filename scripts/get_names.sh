#!/bin/bash

FOLDERS=../volume_importer_xml/*
mkdir -p taxon_names

for folder in $FOLDERS
do
  echo "folder: $folder"
  OUTDIR="$(pwd)/taxon_names/$(basename $folder)"
  echo "OUTDIR: $OUTDIR"
  mkdir -p "$OUTDIR"
  VOLUMES=$folder/*
  for volume in $VOLUMES
  do
    python3 extract_titles.py < "$volume/treatments.xml" > "$OUTDIR/$(basename $volume)_titles.json"
    sed 's/;/ /g' < "$volume/treatments.xml" | gnfinder find -c > "$OUTDIR/$(basename $volume)_gnfound.json"
  done
  # take action on each file. $f store current file name
done
