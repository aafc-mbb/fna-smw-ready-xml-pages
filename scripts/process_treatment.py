import re
from misc import file_to_json
from match import Trie


def get_unitalicize():
    '''
    return function to unitalicize incorrectly italicized words
    '''
    no_italics = set(file_to_json("do_not_italicize.json"))
    no_ital_re = re.compile('|'.join(x.replace('.', '\.')
                                     for x in no_italics), re.DOTALL)

    def helper(ital_text):
        return no_ital_re.sub(lambda x: "</i>" + x.group() + "<i>", ital_text.group())

    return helper


def get_match_function():
    not_ital = re.compile(
        '^.*?(?=<i>)|(?<=</i>).*?(?=<i>)|(?<=</i>).*?$|^.*?$', re.DOTALL)
    un_ital = get_unitalicize()
    ital = re.compile("(?<=<i>).*?(?=</i>)")
    tmatch = Trie(file_to_json("full_list.json"))
    '''
	return function to find and italicize scientific names in a block of text
	'''
    def helper(stval):
        '''
        only process parts that aren't already italicized
        '''
        return ital.sub(un_ital, not_ital.sub(lambda x: tmatch.get_matches(x.group(), lambda y: "<i>" + y + "</i>"), stval.group()))

    return helper


def get_match_function_treatment():
    not_ital = re.compile(
        '^.*?(?=<i>)|(?<=</i>).*?(?=<i>)|(?<=</i>).*?$|^.*?$', re.DOTALL)
    not_bold = re.compile(
        '^.*?(?=<b>)|(?<=</b>).*?(?=<b>)|(?<=</b>).*?$|^.*?$', re.DOTALL)
    un_ital = get_unitalicize()
    ital = re.compile("(?<=<i>).*?(?=</i>)")
    tmatch = Trie(file_to_json("full_list.json"))
    '''
	return function to find and italicize scientific names in a block of text
	'''
    def helper(stval):
        '''
        only process parts that aren't already italicized
        '''
        return ital.sub(un_ital,
                        not_ital.sub(lambda x: not_bold.sub(
                            lambda z: tmatch.get_matches(z.group(), lambda y: "<i>" + y + "</i>"), x.group()), stval.group()))

    return helper


def get_process_treatment():
    discussion_match = re.compile(
        '(?<=\|discussion=).*?(?=\|tables=)', re.DOTALL)
    treatment_match = re.compile(
        '(?<=\|treatment_page=).*?(?=Treatment/Body)', re.DOTALL)
    get_match = get_match_function()
    get_match_treatment = get_match_function_treatment()

    def helper(stval):
        '''
        only process the required sections
        '''
        return treatment_match.sub(get_match_treatment, discussion_match.sub(get_match, stval))
    return helper
