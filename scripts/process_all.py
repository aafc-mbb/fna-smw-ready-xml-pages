from os import path, walk
from process_treatment import *


proc_treatment = get_process_treatment()
l = []
for root, dirs, files in walk("../volume_importer_xml", topdown=False):
    for file in files:
        if file == "treatments.xml":
            # cnt[key_val]-=1
            with open(path.join(root, file), 'r', encoding='utf8') as f:
                res = proc_treatment(f.read())
            with open(path.join(root, file), 'w', encoding='utf8') as f:
                f.write(res)
        else:
            continue
