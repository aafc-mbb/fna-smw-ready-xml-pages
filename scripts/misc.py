import json

from os import path


def file_to_json(filename, file_dir=''):
    '''
    Takes the name of a json file and returns its contents as a dictionary
    filename: a string--the name of a json file
    '''
    
    with open(path.join(file_dir, filename), 'r', encoding='utf-8') as f:  # gn_blacklist.json
        j = json.load(f)  # j is the blacklist
    return j
