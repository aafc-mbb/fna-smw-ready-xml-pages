import sys
import io
from process_treatment import *

proc_treatment = get_process_treatment()

input_stream = io.TextIOWrapper(sys.stdin.buffer, encoding='utf-8')

i = input_stream.read()

sys.stdout.buffer.write(proc_treatment(i).encode('utf8'))
