import re


class Trie:
    '''
    Data structure for fast string matching
    '''

    def __ext_trie(self):
        self.__res.append(False)
        self.__trie.append({})

    def __get_next(self, tind, cind):
        if (cind in self.__trie[tind]):
            return self.__trie[tind][cind]
        else:
            self.__cnt += 1
            self.__ext_trie()
            self.__trie[tind][cind] = self.__cnt
            return self.__cnt

    def __add_key_at_pos(self, s, stind):
        for i in s:
            stind = self.__get_next(stind, i)
        self.__res[stind] = True

    def __add_hybrid(self, s, sind, stind):
        self.__add_key_at_pos(
            s[sind + 1:], self.__get_next(stind, 'x' + s[sind][1:]))
        nxtl = [s[sind][1:]] + s[sind + 1:]
        self.__add_key_at_pos(nxtl, self.__get_next(stind, '×'))
        self.__add_key_at_pos(nxtl, self.__get_next(stind, 'x'))


    def __add_key(self, s):
        stind = 0
        for i in range(len(s)):
            if len(s[i]) > 0 and s[i][0] == '×':  # s = ['Taraxacum', ' ', 'scopulorum']
                # s = ['Crataegus', ' ', '×kelloggii']
                self.__add_hybrid(s, i, stind)
            stind = self.__get_next(stind, s[i])
        self.__res[stind] = True

    def __add_key_val(self, s, val):
        self.__res[self.__add_key(s)] = val

    def __init__(self, input_vals): # gets passed the full_list.json
        '''
        input_vals: list of strings strings
        creates a data structure to return true for all strings that are in the list
        '''
        self.__cnt = 0
        self.__res = []
        self.__trie = []
        self.__ext_trie()
        self.letters = re.compile('([^A-Za-zöïë×éÉ]+)')
        for i in input_vals:
            self.__add_key(self.letters.split(i))

    def __lookup(self, sind, s_vals):
        stind = 0
        match_set = [-1]
        for i in range(sind, len(s_vals)):
            if s_vals[i] in self.__trie[stind]:
                stind = self.__trie[stind][s_vals[i]]
                # this should be modified; currently grabs all, should only grab best
                if self.__res[stind]:
                    match_set.append(i)
            else:
                return match_set[-1]
        return match_set[-1]

    def get_matches(self, s, apply_to_match):
        res = []
        s_vals = self.letters.split(s)
        i = 0
        while i < len(s_vals):
            tmp_ind = self.__lookup(i, s_vals)
            if tmp_ind < 0:
                res.append(s_vals[i])
                i += 1
            else:
                res.append(apply_to_match(''.join(s_vals[i:tmp_ind + 1])))
                i = tmp_ind + 1
        return ''.join(res)
