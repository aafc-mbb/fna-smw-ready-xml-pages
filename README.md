# Flora of North America (FNA)

## The Project

Flora of North America (FNA) presents for the first time, in one published reference source, information on the names, taxonomic relationships, continent-wide distributions, and morphological characteristics of all plants native and naturalized found in North America north of Mexico.

[FNA Semantic MediaWiki (SMW)](http://beta.semanticfna.org/Main_Page) makes the information contained within the FNA accessible on the web for its users to consume and curate. The FNA SMW is a dynamic, evolving, community-supported and up-to-date version of the flora. For more information, please read our [poster](http://beta.floranorthamerica.org/File:FNA_SMW_Poster.pdf)

## The Repository
### What's in this repository?
Here, we keep processed files ready for import into the [FNA Beta Semantic MediaWiki (SMW)](http://beta.semanticfna.org/Main_Page). They have been processed by a custom app that transforms XML documents provided by a natural language parser called [Charaparser](https://github.com/biosemantics/charaparser) into the Semantic MediaWiki XML import format.

Each treatment found on the the [FNA Beta Semantic MediaWiki (SMW)](http://beta.semanticfna.org/Main_Page) corresponds to an XML file in the subdirectory [volume_importer_xml](https://bitbucket.org/aafc-mbb/fna-smw-ready-xml-pages/src/master/volume_importer_xml/). These files are curated by Beatriz Lujan-Toro and Jocelyn Pender. 

Once the FNA SMW launches (i.e., during the post-beta phase), we do not plan to curate the files in this repository. Therefore, the FNA SMW representation, and not the XML files used for initial import, will serve as the canonical source of truth web-based FNA.

### Workflow

Before the XML files are transformed into input files for the SMW Beta, they are parsed by a natural language parser called [Charaparser](https://github.com/biosemantics/charaparser). 

These [parsed files]((https://bitbucket.org/aafc-mbb/fna-fine-grained-xml/src/master/) are the input to our custom XML transformer app. This repository contains the *output* of our custom XML transformer app.

Further details on our workflow are available upon request (see contact information below). See workflow diagram below for an overview.

![FNA Workflow Documentation](FNA_Workflow_Documentation.jpg)

## License
### Overview

Unless otherwise noted, this project's **contents** (i.e., [coarse_grained_fna_xml/]((https://bitbucket.org/aafc-mbb/fna-data-curation/src/master/coarse_grained_fna_xml/)), [documents/]((https://bitbucket.org/aafc-mbb/fna-data-curation/src/master/documents/)), [properties/]((https://bitbucket.org/aafc-mbb/fna-data-curation/src/master/properties/)), [source_fna/]((https://bitbucket.org/aafc-mbb/fna-data-curation/src/master/source_fna/)), [supplemental_data/]((https://bitbucket.org/aafc-mbb/fna-data-curation/src/master/supplemental_data))) are covered under copyright held by the Flora of North America Association. Copyright for volume 24 and 25 contents (V24/ and V25/ subdirectories) is held by Utah State University. These data are distributed under two licenses; (1) a [CC BY License](CC BY License) and (2) a [reuse by request license](Request License). Please see below for more details on license applicability. 

Unless otherwise noted, this project's **source code** (i.e., [scripts/]((https://bitbucket.org/aafc-mbb/fna-data-curation/src/master/scripts/))) are covered under Crown Copyright, Government of Canada, and is distributed under the [MIT License](LICENSE).

For content licensed under [CC BY](CC BY License), please mark and describe changes you make to the material.

If you wish to use this work for commercial purposes, please submit reuse requests to copyright@floranorthamerica.org.

### License Applicability

#### coarse_grained_fna_xml/, supplemental_data/exiled_coarse_grained_fna_xml, supplemental_data/changes_spreadsheet
| Data | License |
|-|-|
| Taxonomy | [CC BY](CC BY License) |
| Morphological description | [CC BY](CC BY License) |
| Distribution | [CC BY](CC BY License) |
| Discussion | Reuse by request at copyright@floranorthamerica.org |

#### documents/
Reuse by request at copyright@floranorthamerica.org

#### properties/
[CC BY](CC BY License)

#### scripts/
[MIT](LICENSE)

#### source_fna/
Reuse by request at copyright@floranorthamerica.org

#### supplemental_data/author_affiliations
[CC BY](CC BY License)

#### supplemental_data/distribution_maps/
Flora of North America legacy map: Reuse by request at copyright@floranorthamerica.org

#### supplemental_data/illustrators/
[CC BY](CC BY License)

#### supplemental_data/references/
[CC BY](CC BY License)

## Attribution

Please help support the Flora of North America project by providing [appropriate attribution](https://wiki.creativecommons.org/wiki/Best_practices_for_attribution).

| Data | Attribution |
|-|-|
| Text material | Author(s) and source |
| Maps | Author(s) and source |
| Illustrations | Artist and source |

For citation instructions, see [How to Cite](http://beta.floranorthamerica.org/How_to_Cite).

## How to Contribute

See [CONTRIBUTING.md](CONTRIBUTING.md).
 
## Contact

* Jocelyn Pender, jocelyn.pender@canada.ca
* Beatriz Lujan-Toro, beatriz.lujan-toro@canada.ca
* Joel Sachs, joel.sachs@canada.ca
* James Macklin, james.macklin@canada.ca
* Geoff Levin, levin1@illinois.edu
 
## Background Information

Please see our [poster](http://beta.floranorthamerica.org/File:FNA_SMW_Poster.pdf) for more background information.
